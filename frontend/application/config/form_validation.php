<?php
    
    $config = array(
             'signup' => array(
                                array(
                                        'field' => 'username',
                                        'label' => 'Username',
                                        'rules' => 'trim|required|min_length[4]|xss_clean'
                                     ),
                                array(
                                        'field' => 'password',
                                        'label' => 'Password',
                                        'rules' => 'trim|required|min_length[6]|xss_clean'
                                     ),
                                array(
                                        'field' => 'confirm',
                                        'label' => 'PasswordConfirmation',
                                        'rules' => 'trim|required|matches[password]|min_length[6]|xss_clean'
                                     ),
                                array(
                                        'field' => 'email',
                                        'label' => 'Email',
                                        'rules' => 'trim|valid_emai|xss_clean'
                                     ),
                                array(
                                        'field' => 'businessname',
                                        'label' => 'Business name',
                                        'rules' => 'trim|required|xss_clean'
                                     ),
                                array(
                                        'field' => 'district',
                                        'label' => 'District',
                                        'rules' => 'trim|required|xss_clean'
                                     ),
                                array(
                                        'field' => 'mobile',
                                        'label' => 'Mobile',
                                        'rules' => 'trim|required|is_natural|exact_length[10]|xss_clean'
                                     ),
                                array(
                                        'field' => 'landmark',
                                        'label' => 'Landmark',
                                        'rules' => 'trim|required|xss_clean'
                                     ),
                                array(
                                        'field' => 'city',
                                        'label' => 'City',
                                        'rules' => 'trim|required|xss_clean'
                                     ),
                                array(
                                        'field' => 'landline',
                                        'label' => 'Landline',
                                        'rules' => 'trim|is_natural|xss_clean'
                                     ),
                                array(
                                        'field' => 'merchantname',
                                        'label' => 'Owners name',
                                        'rules' => 'trim|required|xss_clean'
                                     ),
                                array(
                                        'field' => 'merchantcity',
                                        'label' => 'Owners city',
                                        'rules' => 'trim|required|alpha|xss_clean'
                                     ),
                                array(
                                        'field' => 'merchantmobile',
                                        'label' => 'Owners mobile',
                                        'rules' => 'trim|required|is_natural|exact_length[10]|xss_clean'
                                     ),
                                array(
                                        'field' => 'merchantaddress',
                                        'label' => 'Owners address',
                                        'rules' => 'trim|required|xss_clean'
                                     ),
                                array(
                                        'field' => 'merchantdistrict',
                                        'label' => 'Owners district',
                                        'rules' => 'trim|required|alpha|xss_clean'
                                     )
                                ),
             'email' => array(
                                array(
                                        'field' => 'emailaddress',
                                        'label' => 'EmailAddress',
                                        'rules' => 'required|valid_email'
                                     ),
                                array(
                                        'field' => 'name',
                                        'label' => 'Name',
                                        'rules' => 'required|alpha'
                                     ),
                                array(
                                        'field' => 'title',
                                        'label' => 'Title',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'message',
                                        'label' => 'MessageBody',
                                        'rules' => 'required'
                                     )
                                )                          
           );
?>