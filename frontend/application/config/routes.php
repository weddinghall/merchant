<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

// $route['merchant/login'] = 'merchant/signup/login';
// $route['merchant/registration'] = 'merchant/signup/registration';
// $route['merchant/validate_credentials'] = 'merchant/signup/validate_credentials';
// $route['merchant/validate_registration'] = 'merchant/signup/validate_registration';
// $route['merchant/dashboard'] = 'merchant/dashboard/index';
// $route['merchant/profile'] = 'merchant/profile/index';
// $route['merchant/events'] = 'merchant/events/index';
// $route['merchant/gallery'] = 'merchant/gallery/index';
// $route['merchant/pinboard'] = 'merchant/pinboard/index';
// $route['merchant/contract'] = 'merchant/contract/index';
// $route['merchant/features'] = 'merchant/features/index';
// $route['merchant/gallery/add'] = 'merchant/gallery/add_images';
// $route['merchant/password/change'] = 'merchant/dashboard/change_password';
// $route['merchant/validate_change_password'] = 'merchant/dashboard/validate_change_password';
// $route['merchant/profile/add'] = 'merchant/profile/add_profile';

$route['findmore/(:any)'] = 'home/findmore_auditorium/$i';
$route['details/(:any)'] = 'home/hall_details/$1';
$route['auditorium'] = 'home/search_auditorium';
/* End of file routes.php */
/* Location: ./application/config/routes.php */