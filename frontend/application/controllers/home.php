<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	//var $booking_date;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->model('home_model');
		$this->output->enable_profiler(TRUE);
	}
	
	public function index()
	{
		$newdata = array(
                   'username'  => 'hareesh',
                   'email'     => 'er.hareeshsnair@gmail.com',
                   'booking_date' => '2015-11-11',
                   'logged_in' => TRUE
               );

		$this->session->set_userdata($newdata);
		$data['dist_clt'] = $this->home_model->get_popular_city('Kozhikode');
		$data['dist_tcr'] = $this->home_model->get_popular_city('Thrissur');
		$data['dist_ekm'] = $this->home_model->get_popular_city('Ernakulam');
		$data['dist_tvm'] = $this->home_model->get_popular_city('Thiruvananthapuram');
		$data['sponsers_add'] = $this->home_model->get_sponsers_add();
		
		if($this->session->flashdata('notification'))
		{
			$data[array_keys($this->session->flashdata('notification'))[0]] = $this->session->flashdata('notification');
		}
		$data['main_content'] = 'home/main';
		$data['page_script'] = 'home/script';
		$this->load->view('includes/template', $data);
	}

	public function search_auditorium()
	{
		$dist = $this->input->post('district');
		$location = $this->input->post('location');
		$date = $this->input->post('date');
		$name = $this->input->post('name');
		if ($date != '' OR $date != NULL)
		{
			$date_check = explode('-', $date);
			if($date_check[0]>2014 && $date_check[0]<2018)
			{
				if(isset($_COOKIE))
				$today = strtotime(date('Y-m-d'));
				$book_date = strtotime($date);
				if($book_date > $today)
				{
					$booking_date = $date;
					$data['result'] = $this->home_model->search_auditorium($name,$dist,$location,$date);
					$data['best_sellers'] = $this->home_model->best_sellers($dist);
					$data['top_rated'] = $this->home_model->top_rated();
					$data['search_type'] = 'Auditoriums';
					$data['main_content'] = 'hall_list/main';
					$data['page_script'] = 'hall_list/script';
					//echo "<pre>";print_r($data);exit();
					$this->load->view('includes/template', $data);
				}
				else if($book_date == $today)
				{
					$data['error_msg'] = 'You cant book for today';
					$this->session->set_flashdata('notification',$data);
					redirect('/home/index');
				}
				else
				{
					$data['error_msg'] = 'Invalid date';
					$this->session->set_flashdata('notification',$data);
					redirect('/home/index');
				}
			}	
			else
			{
				$data['error_msg'] = 'Booking available upto 2017 only';
				$this->session->set_flashdata('notification',$data);
				redirect('/home/index');
			}
		}
		else
		{
			$data['error_msg'] = 'Please choose a <strong>Date!</strong>';
			$this->session->set_flashdata('notification',$data);
			redirect('/home/index');
		}
	}
	
	public function hall_details()
	{
		$id = $this->uri->segment(2);
    	if(!ctype_digit($id))
    	{
			echo 'invalid id';
		}
		else if($this->__check_id_exist($id))
		{
			$data['details'] = $this->home_model->get_auditorium_details($id);
			$data['facilities'] = $this->home_model->get_facilities($id);
			$data['foods'] = $this->home_model->types_of_foods($id);
			$data['reviews'] = $this->home_model->get_reviews($id);
			$data['main_content'] = 'hall_details/main';
			$data['page_script'] = 'hall_details/script';
			//echo "<pre>";print_r($data);exit();
			$this->load->view('includes/template', $data);
		}
	}

	function findmore_auditorium()
	{
		$location = $this->uri->segment(2);
		if(isset($location))
		{
			switch($location)
			{
				case 'Kozhikode'	: $this->find_more('Kozhikode'); break;
				case 'Thrissur'		: $this->find_more('Thrissur'); break;
				case 'Kochi'		: $this->find_more('Ernakulam'); break;
				case 'Thiruvananthapuram'	: $this->find_more('Thiruvananthapuram'); break;
				default 			: echo 'Invalid'; break;
			}
		}
		else
		{
			$data['error_msg'] = 'Please choose a <strong>City!</strong>';
			$this->session->set_flashdata('notification',$data);
			redirect('/home/index');
		}
		
	}

	private function __check_id_exist($id)
	{
		$id_exist = $this->home_model->check_id_exist($id);
		if(!empty($id_exist))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function find_more($dist)
	{
		$data['result'] = $this->home_model->find_more($dist);
		//echo "<pre>";print_r($data);exit;
		$data['best_sellers'] = $this->home_model->best_sellers($dist);
		$data['top_rated'] = $this->home_model->top_rated();
		$data['search_type'] = 'Auditoriums';
		$data['main_content'] = 'hall_list/main';
		$data['page_script'] = 'hall_list/script';
		$this->load->view('includes/template', $data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */