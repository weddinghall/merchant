<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->library('email');
		$this->load->model('email_model');
		$this->output->enable_profiler(TRUE);
	}

	public function signup_successful()
	{

	}

	public function email_verification()
	{

	}

	public function mobile_verification()
	{

	}

	public function booking_confirmation()
	{

	}

	public function new_offers()
	{

	}

	public function review_confirmation()
	{

	}

	public function user_feedback()
	{

	}

}

/* End of file email.php */
/* Location: ./application/controllers/email.php */