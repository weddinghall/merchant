<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hall_details extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['hall_name'] = 'Hall name';
		$data['main_content'] = 'hall_details/main';
		$data['page_script'] = 'merchant/dashboard/script';
		$this->load->view('includes/template', $data);
	}

	
}

/* End of file hall_details.php */
/* Location: ./application/controllers/hall_details.php */