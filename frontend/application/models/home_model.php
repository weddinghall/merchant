<?php

class home_model extends CI_Model {

	function get_popular_city($district)
	{
		$this->db->select('business_city,count(id) AS count');
		$this->db->where('business_type', 'Auditorium');
		$this->db->where('business_district', $district);
		$this->db->where('status', 'Active');
		$this->db->group_by('business_city'); 
		
		$query = $this->db->get('business_registration',5,0);
		return $query->result_array();
	}

	function get_sponsers_add()
	{
		$query = $this->db->get_where('sponsers_add',array('status' => 'Active'));
		return $query->result_array();
	}

	/*
	 *Search auditorium through search box of index page
	 */
	function search_auditorium($name=NULL,$dist=NULL,$location=NULL,$date)
	{
		$this->db->select('br.id');
		$this->db->from('business_registration br');
		$this->db->join('booking_details bd', 'br.id = bd.business_id', 'left');
		$this->db->where('bd.event_date =', $date);
				
		$query = $this->db->get();
		$res = $query->result_array();
		$booked_id = array();
		if(!empty($res))
		{
			foreach ($res as $key => $value) 
			{
				$booked_id[] = $value['id'];
			}
		}
		else $booked_id[] = 0;
		
		$this->db->select('br.*,ad.rate_per_day,mc.discount,mc.discount_type');
		$this->db->from('business_registration br');
		$this->db->join('auditorium_details ad', 'br.id = ad.business_id', 'left');
		$this->db->join('merchant_coupons mc', 'br.id = mc.business_id', 'left');
		// $this->db->join('facility f','af.facility_id = f.id','left');
		$this->db->where_not_in('br.id', $booked_id);
		$this->db->where('br.status', 'Active');
		$this->db->where('mc.status', 'Active');
		$this->db->or_where('mc.status',NULL);
		if($name!=NULL)
		{
			$this->db->where('br.business_name', $name);	
		}
		else
		{
			if($dist!=NULL)
			{
				$this->db->where('br.business_district', $dist);
			}
			if($location!=NULL)
			{
				$this->db->where('br.business_city', $location);
			}	
		}

		$query2 = $this->db->get();
		return $query2->result_array();
	}

	function best_sellers($dist=NULL)
	{
		$this->db->select('bd.business_id,count(bd.id) AS count,br.*,ad.rate_per_day,mc.discount,mc.discount_type');
		$this->db->from('booking_details bd');
		$this->db->join('business_registration br', 'br.id = bd.business_id', 'left');
		$this->db->join('auditorium_details ad', 'bd.business_id = ad.business_id', 'left');
		$this->db->join('merchant_coupons mc', 'br.id = mc.business_id', 'left');
		$this->db->where('br.status', 'Active');
		$this->db->where('mc.status', 'Active');
		$this->db->or_where('mc.status',NULL);
		if($dist!=NULL)
		{
			$this->db->where('br.business_district', $dist);
		}
		$this->db->group_by('bd.business_id'); 
		$this->db->order_by('count','DESC');
		
		$query = $this->db->get();
		return $query->result_array();
	}

	function top_rated()
	{
		$this->db->select('r.business_id,br.business_name,br.business_city,ad.rate_per_day, SUM(r.star_rating) AS rating, (SUM(r.star_rating)/COUNT(r.id)) AS star_rating');
		$this->db->from('reviews r');
		$this->db->join('business_registration br', 'br.id = r.business_id', 'left');
		$this->db->join('auditorium_details ad', 'br.id = ad.business_id', 'left');
		$this->db->where('br.status', 'Active');
		$this->db->where('r.status', 'Approved');
		$this->db->group_by('r.business_id'); 
		$this->db->limit(4);
		
		$query = $this->db->get();
		return $query->result_array();
	}

	function check_id_exist($id)
	{
		$this->db->where('id', $id);
		$this->db->where('status', 'Active');
		$query = $this->db->get('business_registration');
		return $query->row();
	}

	function get_auditorium_details($id)
	{
		$this->db->select('br.id as auditorium_id,br.business_name,br.business_location,br.business_city,br.business_district,br.business_email,br.business_mobile,br.business_landline,br.account_type_no');
		$this->db->select('ad.*');
		$this->db->from('business_registration br');
		$this->db->join('auditorium_details ad', 'br.id = ad.business_id', 'left');
		$this->db->where('br.id', $id);
		$this->db->where('br.status', 'Active');

		$query = $this->db->get();
		return $query->row();
		
	}

	function get_facilities($id)
	{
		$this->db->select('f.*');
		$this->db->from('auditorium_facilities af');
		$this->db->join('facility f', 'f.id = af.facility_id', 'left');
		$this->db->where('af.business_id', $id);

		$query = $this->db->get();
		return $query->result_array();
	}

	function types_of_foods($id)
	{
		$this->db->select('t.*');
		$this->db->from('auditorium_foods af');
		$this->db->join('types_of_food t', 't.id = af.food_id', 'left');
		$this->db->where('af.business_id', $id);

		$query = $this->db->get();
		return $query->result_array();
	}

	function get_reviews($id)
	{
		$this->db->select('*');
		$this->db->from('reviews');
		$this->db->where('business_id', $id);

		$query = $this->db->get();
		return $query->result_array();
	}

	function find_more($dist)
	{
		$this->db->select('br.*,ad.rate_per_day,mc.discount,mc.discount_type');
		$this->db->from('business_registration br');
		$this->db->join('auditorium_details ad', 'br.id = ad.business_id', 'left');
		$this->db->join('merchant_coupons mc', 'br.id = mc.business_id', 'left');
		$this->db->where('br.business_district', $dist);
		$this->db->where('br.status', 'Active');
		//$this->db->where("('mc.status' = 'Active' OR 'mc.status' IS NULL)");
		//$this->db->where('mc.status !=','Inactive');

		$query2 = $this->db->get();
		return $query2->result_array();
	}


}