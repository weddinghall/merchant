<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">

							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title"><?php echo $details->business_name; ?></h1>
							<div class="separator-2"></div>
							<!-- page-title end -->

							<div class="row">
								<div class="col-md-8">
									<!-- pills start -->
									<!-- ================ -->
									<!-- Nav tabs -->
									<div class="owl-carousel content-slider-with-controls-autoplay">
										<div class="overlay-container overlay-visible">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-2.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
													<h3 class="title">Caption Title</h3>
												</div>
											</div>
											<a href="<?php echo base_url(); ?>assets/images/portfolio-2.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
										<div class="overlay-container overlay-visible">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-1.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
													<h3 class="title">Caption Title</h3>
												</div>
											</div>
											<a href="<?php echo base_url(); ?>assets/images/portfolio-1.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
										<div class="overlay-container overlay-visible">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
													<h3 class="title">Caption Title</h3>
												</div>
											</div>
											<a href="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
									</div>
									
									<div class="space-bottom"></div>
									
									<div class="owl-carousel carousel-autoplay hidden-xs">
										<div class="overlay-container">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-1.jpg" alt="">
											
										</div>
										<div class="overlay-container">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-2.jpg" alt="">
											
										</div>
										<div class="overlay-container">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" alt="">
											
										</div>
										<div class="overlay-container">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-4.jpg" alt="">
											
										</div>
										<div class="overlay-container">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-5.jpg" alt="">
											
										</div>
										<div class="overlay-container">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-6.jpg" alt="">
											
										</div>
									</div>
									<!-- pills end -->
								</div>
								<div class="col-md-4 pv-30 pv-0">
									<h4>Contact</h4>
									<p class="small">
										<i class="fa fa-map-marker"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $details->business_location.', '.$details->business_city; ?><br>
										<i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;<?php echo $details->business_email; ?><br>
										<i class="fa fa-mobile"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $details->business_mobile; ?><br>
										<i class="fa fa-tty"></i>&nbsp;&nbsp;&nbsp;<?php echo $details->business_landline; ?>
									</p>

									<h4>Description</h4>
									<p><?php echo $details->description; ?>.</p>
									<h4>Facilities</h4>
									<div class="col-xs-12">
									<p class="small">
									<?php
									foreach ($facilities as $key => $facility)
									{
									?>
										<i class="fa fa-<?php echo $facility['fa_icon'];?>">&nbsp;&nbsp;<?php echo $facility['facility'];?></i><br>
										
									<?php
									}
									?>
									</p>
									</div>

									<hr class="mb-10">
									<div class="clearfix mb-20">
										<span>
											<i class="fa fa-star text-default"></i>
											<i class="fa fa-star text-default"></i>
											<i class="fa fa-star text-default"></i>
											<i class="fa fa-star text-default"></i>
											<i class="fa fa-star"></i>
										</span>
										<ul class="pl-20 pull-right social-links circle small clearfix margin-clear animated-effect-1">
											<li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
											<li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
										</ul>
									</div>
									<?php
									if($this->session->userdata('booking_date') == '')
									{
									?>
									<div class="row grid-space-10">
										<form role="form" class="clearfix">
											<div class="col-md-12">
												<div class="form-group">
													<label>Quantity</label>
													<input type="text" class="form-control" value="1">
												</div>
											</div>
											
											<div class="col-md-12 text-right">
												
											</div>
										</form>
									</div>
									<?php } ?>
									<div class="light-gray-bg p-20 bordered clearfix">
										<span class="product price icon-tag">&nbsp;&nbsp;<i class="pr-5 fa fa-inr"></i><?php echo number_format($details->rate_per_day); ?></span>
										<?php if($details->account_type_no > 0)
										{
										?>
										<div class="product elements-list pull-right clearfix">
											<input type="submit" value="Book Now" class="margin-clear btn btn-default">
										</div>
										<?php
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<!-- main end -->

					</div>
				</div>
			</section>
			<!-- main-container end -->

			<!-- section start -->
			<!-- ================ -->
			<section class="pv-30 light-gray-bg">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs style-4" role="tablist">
								<li class="active"><a href="#h2tab2" role="tab" data-toggle="tab"><i class="fa fa-files-o pr-5"></i>Specifications</a></li>
								<li><a href="#h2tab3" role="tab" data-toggle="tab"><i class="fa fa-star pr-5"></i>(3) Reviews</a></li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content padding-top-clear padding-bottom-clear">
								<div class="tab-pane fade in active" id="h2tab2">
									<h4 class="space-top">Specifications</h4>
									<hr>
									<dl class="dl-horizontal">
										<dt>Total area</dt>
										<dd><?php echo $details->area; ?> Sq.ft.</dd>
										<dt>No of floors</dt>
										<dd><?php echo $details->floors; ?></dd>
										<dt>Seating capacity</dt>
										<dd><?php echo $details->seating_capacity; ?> Seats</dd>
										<dt>Dining capacity</dt>
										<dd><?php echo $details->dining_capacity; ?> Seats</dd>
										<dt>Parking capacity</dt>
										<dd><?php echo $details->parking_capacity; ?> Vehicles</dd>
										<dt>Rate per day</dt>
										<dd><?php echo 'Rs. '.number_format($details->rate_per_day); ?>/-</dd>
										<dt>Advance</dt>
										<dd><?php echo 'Rs. '.number_format($details->advance_amt); ?>/-</dd>
										<dt>Types of food</dt>
										<?php foreach ($foods as $key => $foods) 
										{
											echo '<dd>'.$foods['food_type'].'</dd>';
										}
										?>
																				
									</dl>
									<hr>
								</div>
								<div class="tab-pane fade" id="h2tab3">
									<!-- comments start -->
									<div class="comments margin-clear space-top">
										<!-- comment start -->
										<?php
										foreach ($reviews as $key => $review)
										{
										?>
										<div class="comment clearfix">
											<div class="comment-avatar">
												<img class="img-circle" src="images/avatar.jpg" alt="avatar">
											</div>
											<header>
												<h4><?php echo $review['user_name']; ?></h4>
												<div class="comment-meta">
												<?php
												for($i=1;$i<=5;$i++)
										  		{
											  		if($i <= $review['star_rating'])
													echo '<i class="fa fa-star text-default"></i>';
													else
													echo '<i class="fa fa-star"></i>';
												}
												?>
													| <?php echo date("d-m-Y",strtotime($review['date_added'])); ?>
												</div>
											</header>
											<div class="comment-content">
												<div class="comment-body clearfix">
													<p><?php echo $review['review']; ?> </p>
												</div>
											</div>
										</div>
										<!-- comment end -->
										<?php
										}
										?>
										
									</div>
									<!-- comments end -->

									<!-- comments form start -->
									<div class="comments-form">
										<h2 class="title">Add your Review</h2>
										<form role="form" id="comment-form">
											<div class="form-group has-feedback">
												<label for="name4">Name</label>
												<input type="text" class="form-control" id="name4" placeholder="" name="name4" required>
												<i class="fa fa-user form-control-feedback"></i>
											</div>
											<div class="form-group">
												<label>Rating</label>
												<select class="form-control" id="review">
													<option value="five">5</option>
													<option value="four">4</option>
													<option value="three">3</option>
													<option value="two">2</option>
													<option value="one">1</option>
												</select>
											</div>
											<div class="form-group has-feedback">
												<label for="message4">Message</label>
												<textarea class="form-control" rows="8" id="message4" placeholder="" name="message4" required></textarea>
												<i class="fa fa-envelope-o form-control-feedback"></i>
											</div>
											<input type="submit" value="Submit" class="btn btn-default">
										</form>
									</div>
									<!-- comments form end -->
								</div>
							</div>
						</div>

						<!-- sidebar start -->
						<!-- ================ -->
						
					</div>
				</div>
			</section>
			<!-- section end -->
