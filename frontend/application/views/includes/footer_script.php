		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/modernizr.js"></script>

		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/waypoints/jquery.waypoints.min.js"></script>

		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl.carousel.js"></script>
		<!-- Contact form -->
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.validate.js"></script>

		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>
	