<?php 

	$this->load->view('includes/header');
	
	$this->load->view('includes/nav_bar');
	
	$this->load->view($main_content);

	$this->load->view('includes/footer');

	$this->load->view('includes/footer_script');	
	
	//$this->load->view('includes/individual_script');
	
	$this->load->view($page_script); 

?>