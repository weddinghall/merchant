<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-9">
							<!-- page-title start -->
							<!-- ================ -->
							<h2 class="page-title"><?php echo $search_type; ?></h2>
							<div class="separator-2"></div>
							<!-- page-title end -->
							<!-- pills start -->
							<!-- ================ -->
							<!-- Tab panes -->
										
									<div class="row masonry-grid-fitrows grid-space-10 min-hgt">
									<?php 
									if(!empty($result))
									{
										foreach($result as $hall)
										{
									?>
									
										<div class="col-xs-6 col-sm-4 col-lg-4 masonry-grid-item">
											<div class="listing-item white-bg bordered mb-20">
												<div class="overlay-container">
													<img src="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" alt="">
													<a class="overlay-link popup-img-single" href="<?php echo base_url(); ?>assets/images/portfolio-3.jpg"><i class="fa fa-search-plus"></i></a>
													<span class="badge"><?php if(isset($hall['discount'])){if($hall['discount_type'] == 'Rs'){echo 'Rs ';}echo $hall['discount'].' OFF';} ?></span>
													
												</div>
												<div class="body">
													<h3 class="xssmall"><a href="shop-product.html"><?php echo $hall['business_name']; ?></a></h3>
													<p class="small">
														<i class="fa fa-map-marker"></i> <?php echo $hall['business_location'].', '.$hall['business_city']; ?>
													</p>
													Facilities:
													<p class="small">
														<i class="fa fa-wifi"></i></a>&nbsp;&nbsp;
														<i class="fa fa-bed"></i>&nbsp;&nbsp;
														<i class="fa fa-cab"></i>&nbsp;&nbsp;
														<i class="fa fa-cutlery"></i>&nbsp;&nbsp;
														<i class="fa fa-recycle"></i>&nbsp;&nbsp;
														<i class="fa fa-credit-card"></i>&nbsp;&nbsp;
														<i class="fa fa-ambulance"></i>&nbsp;&nbsp;
														<i class="fa fa-medkit"></i>&nbsp;&nbsp;
														<i class="fa fa-user-md"></i>&nbsp;&nbsp;
														<i class="fa fa-wheelchair"></i>
													</p>
													<div class="elements-list clearfix">
														<span class="price fa fa-inr"><?php echo number_format($hall['rate_per_day']); ?></span>
														<a href="<?php echo site_url(); ?>/details/<?php echo $hall['id']; ?>" class="pull-right margin-clear btn btn-sm btn btn-default btn-sm btn-hvr hvr-sweep-to-right margin-clear btn-animated xs-btn-full">View Details<i class="fa fa-share-square-o"></i></a>
													</div>
												</div>
											</div>
										</div>
									<?php
										}
									}
									else
									{
										echo 'Auditoriums not found';
									}
									?>
										
									</div>
							<!-- pills end -->
							<!-- pagination start -->
							<nav class="text-center">
								<ul class="pagination">
									<li><a href="#" aria-label="Previous"><i class="fa fa-angle-left"></i></a></li>
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li><a href="#" aria-label="Next"><i class="fa fa-angle-right"></i></a></li>
								</ul>
							</nav>
							<!-- pagination end -->
						</div>
						<!-- main end -->

						<!-- sidebar start -->
						<!-- ================ -->
						<aside class="col-md-3 col-sm-3 ">
							<div class="sidebar">
								<div class="block clearfix">
									<h3 class="title">Best Sellers</h3>
									<div class="separator-2"></div>
									<div id="carousel-sidebar" class="carousel slide" data-ride="carousel">
										<!-- Indicators -->
									
										<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
											
											<?php 
											if(!empty($best_sellers))
											{
												foreach($best_sellers as $key => $best_sellers)
												{ if($key==0) $c = 'active'; else $c = '';
											?>
											
												<div class="item <?php echo $c;?>">
													<div class="listing-item">
														<div class="overlay-container">
															<img src="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" alt="product 1">
															<span class="badge"><?php if(isset($best_sellers['discount'])){if($best_sellers['discount_type'] == 'Rs'){echo 'Rs ';}echo $best_sellers['discount'].' OFF';} ?></span>
															<a href="shop-product.html" class="overlay-link"><i class="fa fa-link"></i></a>
														</div>
														<h3><a href="shop-product.html"><?php echo $best_sellers['business_name']; ?></a></h3>
														<p class="small lite-margin">
															<i class="fa fa-map-marker">&nbsp;&nbsp;<?php echo $best_sellers['business_city']; ?></i>
														</p>
														<span class="price fa fa-inr"><?php echo number_format($best_sellers['rate_per_day']); ?></span>
													</div>
												</div>
											<?php
												}
											}
											?>
											
										</div>
									</div>
								</div>
								<div class="block clearfix small-margin">
									<h3 class="title">Top Rated</h3>
									<div class="separator-2"></div>
									<?php
									foreach($top_rated as $top)
									{
									?>
									<div class="media margin-clear">
										<div class="media-left">
											<div class="overlay-container">
												<img class="media-object" src="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" alt="blog-thumb">
											</div>
										</div>
										<div class="media-body">
											<h6 class="media-heading"><a href="shop-product.html"><?php echo $top['business_name']; ?></a></h6>
											<p class="small no-margin">
												<i class="fa fa-map-marker">&nbsp;&nbsp;<?php echo $top['business_city']; ?></i>
											</p>
											<p class="margin-clear">
												<?php
												for($i=1;$i<=5;$i++)
										  		{
											  		if($i <= $top['star_rating'])
													echo '<i class="fa fa-star text-default fa-pad"></i>';
													else
													echo '<i class="fa fa-star fa-pad"></i>';
												}
												?>
											</p>
											<p class="price fa fa-inr"><?php echo $top['rate_per_day']; ?></p>
										</div>
										<hr>
									</div>
									<?php
									}
									?>
									
								</div>
								
							</div>
						</aside>
						<!-- sidebar end -->
					</div>				
				</div>
			</section>
			<!-- main-container end -->	
			