			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container min-pad">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">							
							<?php if(isset($error_msg))
					        { 
					        ?>
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								<i class="fa fa-warning"></i>&nbsp;&nbsp;<?php echo $error_msg['error_msg']; ?>
							</div>
							<?php 
        					}
        					?>
							<div class="image-box style-4 light-gray-bg">
								<div class="row grid-space-0">
									<div class="col-md-6 col-sm-6">
										<div class="body">
											<div class="col-md-12 padd">
												<label>Search your auditorium</label>
											</div>	
											<form name="search" id="form-search" method="POST" action="<?php echo site_url('auditorium');?>">
											<div class="col-md-12  xs-text">
												<div class="form-group has-feedback">
													<input type="text" class="form-control" name="name" placeholder="Auditorium name">
													<i class="fa fa-tags form-control-feedback"></i>
												</div>
											</div>	
											<div class="col-md-12  xs-text">
												<div class="form-group has-feedback">
													<input type="text" class="form-control" name="district" placeholder="District">
													<i class="fa fa-home form-control-feedback"></i>
												</div>
											</div>
											<div class="col-md-12  xs-text">
												<div class="form-group has-feedback">
													<input type="text" class="form-control" name="location" placeholder="Location">
													<i class="fa fa-map-marker form-control-feedback"></i>
												</div>
											</div>
											<div class="col-md-12  xs-text">
												<div class="form-group has-feedback">
													<input type="text" id="date" class="form-control" name="date" placeholder="Date of booking">
													<i class="fa fa-calendar form-control-feedback"></i>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group has-feedback">
													<input type="submit" id="submit" class="btn btn-default margin-clear xsfull" value="Search Availability">
													<!--a href="<?php// echo site_url('auditorium');?>" id="search_button" class="btn btn-default btn-hvr hvr-shutter-out-horizontal margin-clear xsfull">Search<i class="fa fa-search pl-10"></i></a>
												--></div>											
											</div>
											</form>
										</div>
									</div>
									
									<div class="col-md-6 col-sm-6">
										<div class="owl-carousel content-slider-with-controls-autoplay">
											<?php
											foreach ($sponsers_add as $key => $add)
											{
											?>
											<div class="overlay-container overlay-visible">
												<img src="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" alt="">
												<div class="overlay-bottom hidden-xs">
													<div class="text">
														<h3 class="title"><?php echo $add['sponser_name'].' - '.$add['add_title'];?> </h3>
														<p><?php echo $add['add_description'];?></p>
													</div>
												</div>
												<a href="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
											</div>
											<?php
											}
											?>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						<!-- main end -->

					</div>
					</div>

					<div class="container">
						<section class="section">
						<h3>Latest <span class="text-default">Offers</span></h3>
							<div class="row grid-space-10">
								<div class="col-sm-3 col-md-3 col-xs-6">
									<div class="pv-30 ph-20 white-bg feature-box bordered text-center">
										<span class="icon default-bg circle"><i class="fa fa-plus-square"></i></span>
										<h3>Since</h3>
										
									</div>
								</div>
								<div class="col-sm-3 col-md-3 col-xs-6">
									<div class="pv-30 ph-20 white-bg feature-box bordered text-center">
										<span class="icon default-bg circle"><i class="fa fa-hospital-o"></i></span>
										<h3>Offers</h3>
										
									</div>
								</div>
								<div class="col-sm-3 col-md-3 col-xs-6">
									<div class="pv-30 ph-20 white-bg feature-box bordered text-center">
										<span class="icon default-bg circle"><i class="fa fa-ambulance"></i></span>
										<h3>Calls</h3>
										
									</div>
								</div>
								<div class="col-sm-3 col-md-3 col-xs-6">
									<div class="pv-30 ph-20 white-bg feature-box bordered text-center">
										<span class="icon default-bg circle"><i class="glyphicon glyphicon-time"></i></span>
										<h3>Hours</h3>
										
									</div>
								</div>
							</div>
						</section>
						<section class="section">
						<h3>Popular <span class="text-default">Cities</span></h3>
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="image-box style-2 mb-20 bordered light-gray-bg">
										<div class="overlay-container overlay-visible">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-1.jpg" alt="">
											<div class="overlay-bottom text-left overlay-min-pad">
												<p class="lead margin-clear">Kozhikode</p>
											</div>
										</div>
										<div class="cities">
											<div class="col-sm-8 col-xs-8 col-md-8 minimum-hgt">
											<p class="small mb-15">
											<?php if(!empty($dist_clt))
												{ 
													foreach ($dist_clt as $value) {
														echo '<i class="fa fa-map-marker"></i>&nbsp;'.$value['business_city'].'<br>';
													}
												?>
											</p>
											</div>
											<div class="col-sm-4 col-xs-4 col-md-4">
											<p class="small mb-15">
												<?php 
												foreach ($dist_clt as $value) {
														echo $value['count'].'&nbsp;Halls<br>';
													}
												} ?>
											</p>
											</div>
										</div>
										<div class="body">
											<a href="<?php echo site_url('findmore');?>/Kozhikode" class="btn btn-default btn-sm btn-hvr hvr-sweep-to-right margin-clear">Find More<i class="fa fa-arrow-right pl-10"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="image-box style-2 mb-20 bordered light-gray-bg">
										<div class="overlay-container overlay-visible">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-2.jpg" alt="">
											<div class="overlay-bottom text-left overlay-min-pad">
												<p class="lead margin-clear">Thrissur</p>
											</div>
										</div>
										<div class="cities">
											<div class="col-sm-8 col-xs-8 minimum-hgt">
											<p class="small mb-15">
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
											</p>
											</div>
											<div class="col-sm-4 col-xs-4">
											<p class="small mb-15">
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
											</p>
											</div>
										</div>
										<div class="body">
											<a href="<?php echo site_url('findmore');?>/Thrissur" class="btn btn-default btn-sm btn-hvr hvr-sweep-to-right margin-clear">Find More<i class="fa fa-arrow-right pl-10"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="image-box style-2 mb-20 bordered light-gray-bg">
										<div class="overlay-container overlay-visible">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" alt="">
											<div class="overlay-bottom text-left overlay-min-pad">
												<p class="lead margin-clear">Kochi</p>
											</div>
										</div>
										<div class="cities">
											<div class="col-sm-8 col-xs-8 minimum-hgt">
											<p class="small mb-15">
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
											</p>
											</div>
											<div class="col-sm-4 col-xs-4">
											<p class="small mb-15">
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
											</p>
											</div>
										</div>
										<div class="body">
											<a href="<?php echo site_url('findmore');?>/Kochi" class="btn btn-default btn-sm btn-hvr hvr-sweep-to-right margin-clear">Find More<i class="fa fa-arrow-right pl-10"></i></a>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="image-box style-2 mb-20 bordered light-gray-bg">
										<div class="overlay-container overlay-visible">
											<img src="<?php echo base_url(); ?>assets/images/portfolio-3.jpg" alt="">
											<div class="overlay-bottom text-left overlay-min-pad">
												<p class="lead margin-clear">Thiruvananthapuram</p>
											</div>
										</div>
										<div class="cities">
											<div class="col-sm-8 col-xs-8 minimum-hgt">
											<p class="small mb-15">
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
												<i class="fa fa-map-marker"></i> Web Design<br>
											</p>
											</div>
											<div class="col-sm-4 col-xs-4">
											<p class="small mb-15">
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
												12 Halls<br>
											</p>
											</div>
										</div>
										<div class="body">
											<a href="<?php echo site_url('findmore');?>/Thiruvananthapuram" class="btn btn-default btn-sm btn-hvr hvr-sweep-to-right margin-clear">Find More<i class="fa fa-arrow-right pl-10"></i></a>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section class="section stats">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
									<div class="feature-box pv-20 object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
										<span class="icon dark-bg circle"><i class="fa fa-map-marker"></i></span>
										<h3>Locations</h3>
										<span class="counter" data-to="135" data-speed="2000">0</span>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
									<div class="feature-box pv-20 object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
										<span class="icon dark-bg circle"><i class="fa fa-users"></i></span>
										<h3>Clients</h3>
										<span class="counter" data-to="325" data-speed="2000">0</span>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
									<div class="feature-box pv-20 object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
										<span class="icon dark-bg circle"><i class="fa fa-shield"></i></span>
										<h3>Bookings</h3>
										<span class="counter" data-to="1235" data-speed="2000">0</span>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
									<div class="feature-box pv-20 object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
										<span class="icon dark-bg circle"><i class="fa fa-share-alt"></i></span>
										<h3>Shares</h3>
										<span class="counter" data-to="1002" data-speed="2000">0</span>
									</div>
								</div>
							</div>
						</section>
					</div>		
				</div>
			</section>
			<!-- main-container end -->	
