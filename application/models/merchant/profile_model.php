<?php

class Profile_model extends CI_Model {

	function get_details($id)
	{
		$query = $this->db->get_where('business_registration', array('id' => $id), 1, 0);
		return $query->row();
	}

	function get_adv_details($id)
	{
		$query = $this->db->get_where('auditorium_details', array('business_id' => $id), 1, 0);
		return $query->row();
	}

	function get_merchant_details($id)
	{
		$this->db->select('merchant_name');
		$this->db->from('merchant_registration');
		$this->db->join('business_registration', 'business_registration.merchant_id = merchant_registration.id');

		$query = $this->db->get();
		return $query->row()->merchant_name;
	}

	function last_accessed($id)
	{
		$this->db->select_max('login_date');
		$this->db->where('business_id', $id);

		$query = $this->db->get('business_activity_log');
		return $query->row()->login_date;
	}

	function edit_features($details)
	{
		if($this->session->userdata('business_id')>0)
		{
			$data = array(
               'area' => $details['area'],
               'floors' => $details['floors'],
               'seating_capacity' => $details['seating'],
               'dining_capacity' => $details['dining'],
               'parking_capacity' => $details['parking'],
               'types_of_food' => $details['food_types']
               //'facilities' => $details['facilities']
            );

			$this->db->where('business_id', $this->session->userdata('business_id'));
			if($this->db->update('auditorium_details', $data))	
			{
				$result['status'] = TRUE;
			}
			else
			{
				$result['status'] = FALSE;
				$result['error_type'] = 'NORMAL';
				$result['error_msg'] = 'Unable to save changes';
			}
		
		}
		else
		{
			$result['status'] = FALSE;
			$result['error_type'] = 'FRAUD';
			$result['error_msg'] = 'Fradulent attempt';
		}
		return $result;
	}

}