<?php

class Dashboard_model extends CI_Model {

	function validate($business_email,$password)
	{
		$this->db->where('business_email', $business_email);
		$this->db->where('business_password', $password);
		$this->db->where('status !=', 'Inactive');
		$this->db->limit(1);
		$query = $this->db->get('business_registration');
		
		if($query->num_rows == 1)
		{
			$this->db->select('id, business_name, business_type, account_type, account_type_no');
			$this->db->from('business_registration');
			$this->db->where('business_email', $business_email); 
			$query = $this->db->get();
			$row = $query->row(); 
			return $row;
		}
		else
		{
			$this->db->where('business_username', $business_email);
			$this->db->where('business_password', $password);
			$this->db->where('status !=', 'Inactive');
			$query = $this->db->get('business_registration');
			
			if($query->num_rows == 1)
			{
				$this->db->select('id, business_name, business_type, account_type, account_type_no');
				$this->db->from('business_registration');
				$this->db->where('business_username', $business_email); 
				$query = $this->db->get();
				$row = $query->row(); 
				//print_r($row);exit;
				return $row;
			}
			else
			{
				return FALSE;
			}
		}
	}

	function __encrip_password($password)
    {
        return do_hash($password, 'md5');
    }

	function register($data)
	{
		$this->load->helper('date');
		$password = $this->__encrip_password(trim($data['password']));
		if($this->agent->is_mobile())
		{
			$device = $this->agent->mobile();
		}
		else $device = 'Web';
		$query = $this->db->get_where('business_registration', array('business_username' => trim($data['username'])), 1, 0);
		if($query->num_rows() == 1)
		{
			$status['reg_status'] = FALSE;
			$status['error_msg'] = 'Username already exist! Please choose another.';
		}
		else
		{
			$this->db->trans_begin();
			$insert_data = array(
				'merchant_name' => humanize(trim($data['merchantname'])),
				'merchant_address' => humanize(trim($data['merchantaddress'])),
				'merchant_city' => humanize(trim($data['merchantcity'])),
				'merchant_district' => humanize(trim($data['merchantdistrict'])),
				'merchant_phone' => trim($data['merchantmobile']),
				'reg_date' => date('Y:m:d H:i:s'),
				'reg_ip' => $this->input->ip_address(),
				'reg_device' => $device,
				'reg_os' => $this->agent->platform(),
				'reg_browser' => $this->agent->browser().' '.$this->agent->version(),
				'reg_location' => 'Vatakara'
				);
			$this->db->insert('merchant_registration',$insert_data);
			$merchant_id = $this->db->insert_id();
			$business_data = array(
				'merchant_id' => $merchant_id,
				'business_name' => humanize(trim($data['businessname'])),
				'business_username' => trim($data['username']),
				'business_password' => $password,
				'business_type' => trim($data['businesstype']),
				'business_location' => humanize(trim($data['landmark'])),
				'business_city' => humanize(trim($data['city'])),
				'business_district' => $data['district'],
				'business_state' => 'Kerala',
				'business_email' => trim($data['email']),
				'business_mobile' => trim($data['mobile']),
				'business_landline' => trim($data['landline']),
				'business_reg_date' => date('Y:m:d H:i:s')
				);
			$this->db->insert('business_registration',$business_data);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			    $status['reg_status'] = FALSE;
				$status['error_msg'] = 'Registration could not complete! Please try again.';
				
			    //echo 'rollback';
			}
			else
			{
			    $this->db->trans_commit();
			    $status['reg_status'] = TRUE;
				$status['success_msg'] = 'Registration successful.';
				
			    //echo 'commit';
			}
		}
		return $status;
	}

	function validate_change_password($data)
	{
		$cur_password = $this->__encrip_password(trim($data['cur_password']));

		$this->db->where('id', $this->session->userdata('business_id'));
		$this->db->where('business_password', $cur_password);
		$this->db->where('status !=', 'Inactive');
		$this->db->limit(1);
		$query = $this->db->get('business_registration');
		
		if($query->num_rows == 1)
		{
			$new_password = $this->__encrip_password(trim($data['new_password']));
			$update = array('business_password' => $new_password);
			$this->db->where('id', $this->session->userdata('business_id'));
			if($this->db->update('business_registration', $update))
			{
				$status = TRUE;
			}
			else $status = FALSE;
		}
		else $status = FALSE;

		return $status;
	}

	function log_entry($data)
	{
		if($this->db->insert('business_activity_log', $data))
		{
			return TRUE;
		}
		else return FALSE;
	}

	function error_log($data)
	{
		$this->db->insert('error_log', $data);
	}

	function ajax_username_validate($username)
	{
		$query = $this->db->get_where('business_registration', array('business_username' => $username), 1, 0);
		if($query->num_rows() == 1)
		{
			$res = FALSE;
		}
		else $res = TRUE;
		return $res;
	}

	function visitors_count($id)
	{
		if(isset($id))
		{
			$query = $this->db->get_where('auditorium_details', array('business_id' => $id), 1, 0);
			if($query->num_rows == 1)
			{
				return $query->row()->visitors_count;
			}
			else return FALSE;
		}
	}

	function booking_count($id)
	{
		if(isset($id))
		{
			$this->db->where('business_id', $id);
			$this->db->from('booking_details');
			return $this->db->count_all_results();
		}
	}

	function income($id)
	{
		if(isset($id))
		{
			$this->db->select_sum('booking_amount','income');
			$this->db->where('business_id', $id);
			$query = $this->db->get('booking_details');
			return $query->row()->income;
		}
	}

	function next_event($id)
	{
		if(isset($id))
		{
			$this->db->select('event_type, event_date, invoice_no');
			$this->db->where('business_id', $id);
			$this->db->where('booking_status', 'Success');
			$this->db->where('event_date >=', date('Y-m-d'));
			$this->db->order_by("event_date", "asc"); 
			$this->db->limit(1);
			$query = $this->db->get('booking_details');
			return $query->row();
		}
		else return FALSE;
	}
}