<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->model('merchant/dashboard_model');
		if(!$this->session->userdata('is_merchant_logged_in'))
        {
           redirect('/merchant/login');
        }
	}

	public function index()
	{
		$id = $this->session->userdata('business_id');
		$data['visitors'] = $this->dashboard_model->visitors_count($id);
		$data['booking'] = $this->dashboard_model->booking_count($id);
		$data['income'] = $this->dashboard_model->income($id);
		$data['nextevent'] = $this->dashboard_model->next_event($id);
		if($this->session->flashdata('notification'))
		{
			$data[array_keys($this->session->flashdata('notification'))[0]] = $this->session->flashdata('notification');
		}
		$data['main_content'] = 'merchant/dashboard/main';
		$data['page_script'] = 'merchant/dashboard/script';
		$this->load->view('merchant/includes/template', $data);
	}

	function change_password()
	{
		if($this->session->flashdata('notification'))
		{
			$data[array_keys($this->session->flashdata('notification'))[0]] = $this->session->flashdata('notification');
		}
		$data['main_content'] = 'merchant/dashboard/changepassword';
		$data['page_script'] = 'merchant/dashboard/script';
		$this->load->view('merchant/includes/template', $data);
	}

	function validate_change_password()
	{
		if(!empty($this->input->post()))
		{
			if(trim($this->input->post('cur_password')) != '' && trim($this->input->post('new_password')) != '' && trim($this->input->post('re_password')) != '')
			{
				if(trim($this->input->post('new_password')) == trim($this->input->post('re_password')))
				{
					$result = $this->dashboard_model->validate_change_password($this->input->post());
					if($result)
					{
						$data['success_msg'] = 'Password updated';
						$this->session->set_flashdata('notification',$data);
						redirect('/merchant/password/change');
					}
					else
					{
						$data['error_msg'] = 'Details mught be incorrect! Try again';
						$this->session->set_flashdata('notification',$data);
						redirect('/merchant/password/change');
					}
				}
				else
				{
					$data['error_msg'] = 'New password should match with retype password!';
					$this->session->set_flashdata('notification',$data);
					redirect('/merchant/password/change');
				}
			}
			else
			{
				$data['error_msg'] = 'Submit the password details';
				$this->session->set_flashdata('notification',$data);
				redirect('/merchant/password/change');
			}
		} 
		else show_404();
	}

	public function logout()
	{ 
		 $newdata = array(
		  	'user_name' => NULL,
	      	'business_id' => NULL,
	      	'business_name' => NULL,
	      	'business_type' => NULL,
	      	'account_type_no' => NULL,
		  	'is_merchant_logged_in'  => FALSE,
		);     
		$this->session->unset_userdata($newdata);
		redirect('merchant/login');
	}
}

/* End of file dashboard.php */
/* Location: ./application/controllers/merchant/dashboard.php */