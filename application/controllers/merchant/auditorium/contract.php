<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contract extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('merchant/profile_model');
		//$this->load->model('merchant/contract_model');
		if(!$this->session->userdata('is_merchant_logged_in'))
        {
           redirect('/merchant/login');
        }
	}

	public function index()
	{
		$data['profile'] = $this->profile_model->get_details($this->session->userdata('business_id'));
		$data['merchant'] = $this->profile_model->get_merchant_details($data['profile']->merchant_id);
		$data['last_accessed'] = $this->profile_model->last_accessed($this->session->userdata('business_id'));
		//print_r($data['last_accessed']);exit;
		$data['main_content'] = 'merchant/contract/main';
		$data['page_script'] = 'merchant/dashboard/script';
		$this->load->view('merchant/includes/template', $data);
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/merchant/dashboard.php */