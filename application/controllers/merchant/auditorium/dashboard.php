<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('merchant/dashboard_model');
		if(!$this->session->userdata('is_merchant_logged_in'))
        {
           redirect('/merchant/login');
        }
	}

	public function index()
	{
		$id = $this->session->userdata('business_id');
		$data['visitors'] = $this->dashboard_model->visitors_count($id);
		$data['booking'] = $this->dashboard_model->booking_count($id);
		$data['income'] = $this->dashboard_model->income($id);
		//print_r($data['visitors']);exit;
		$data['main_content'] = 'merchant/dashboard/main';
		$data['page_script'] = 'merchant/dashboard/script';
		$this->load->view('merchant/includes/template', $data);
	}

	public function logout()
	{ 
		 $newdata = array(
		  	'user_name' => NULL,
	      	'business_id' => NULL,
	      	'business_name' => NULL,
	      	'business_type' => NULL,
	      	'account_type_no' => NULL,
		  	'is_merchant_logged_in'  => FALSE,
		);     
		$this->session->unset_userdata($newdata);
		redirect('merchant/login');
	}
}

/* End of file dashboard.php */
/* Location: ./application/controllers/merchant/dashboard.php */