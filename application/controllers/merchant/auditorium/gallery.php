<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('merchant/dashboard_model');
		if(!$this->session->userdata('is_merchant_logged_in'))
        {
           redirect('/merchant/login');
        }
	}

	public function index()
	{
		$data['main_content'] = 'merchant/gallery/main';
		$data['page_script'] = 'merchant/dashboard/script';
		$this->load->view('merchant/includes/template', $data);
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/merchant/dashboard.php */