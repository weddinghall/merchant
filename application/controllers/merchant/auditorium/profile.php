<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('merchant/profile_model');
		if(!$this->session->userdata('is_merchant_logged_in'))
        {
           redirect('/merchant/login');
        }
	}

	public function index()
	{
		$data['profile'] = $this->profile_model->get_details($this->session->userdata('business_id'));
		$data['details'] = $this->profile_model->get_adv_details($this->session->userdata('business_id'));
		$data['main_content'] = 'merchant/profile/main';
		$data['page_script'] = 'merchant/dashboard/script';
		$this->load->view('merchant/includes/template', $data);
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/merchant/dashboard.php */