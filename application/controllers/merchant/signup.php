<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->helper('security');
		$this->load->model('merchant/dashboard_model');
		if($this->session->userdata('is_merchant_logged_in'))
		{
			redirect('/merchant/dashboard/','refresh');
		}
	}

	public function login()
	{
		$data = $this->session->flashdata('notification');
		$this->load->view('merchant/login',$data);
	}

	public function validate_credentials()
	{
		$business_email = trim($this->input->post('email', TRUE));
		$password 		= trim($this->input->post('password', TRUE));
		if(! $business_email OR ! $password)
		{
			$data['error_msg'] = 'Email or Password should not be blank';
			$this->session->set_flashdata('notification',$data);
			redirect('/merchant/login');
		}
		else
		{
			$password = $this->__encrip_password(trim($this->input->post('password', TRUE)));
			$is_valid = $this->dashboard_model->validate($business_email,$password);
			
			if(!$is_valid)
			{
				$data['error_msg'] = 'Invalid Email or Password';
	            $this->session->set_flashdata('notification',$data);
	            redirect('/merchant/login');

			}
	        else // correct username or password
	        {
	            $data = array(
	                'user_name' => $business_email,
	              	'business_id' => $is_valid->id,
	              	'business_name' => $is_valid->business_name,
	              	'business_type' => $is_valid->business_type,
	              	'account_type_no' => $is_valid->account_type_no,
	                'is_merchant_logged_in' => TRUE
	            );
	            $this->session->set_userdata($data);
	            $this->__log_entry($is_valid->id);
	            redirect('/merchant/dashboard/','refresh');
	        }
		}
	}

	function __encrip_password($password)
    {
        return do_hash($password, 'md5');
    }

    private function __log_entry($id)
    {
    	if($this->agent->is_mobile())
		{
			$device = $this->agent->mobile();
		}
		else $device = 'Web';
		$data = array(
					'business_id' => $id,
					'login_date' => date('Y:m:d H:i:s'),
					'login_ip' => $this->input->ip_address(),
					'login_device' => $device,
					'login_os' => $this->agent->platform(),
					'login_browser' => $this->agent->browser().' '.$this->agent->version(),
					'login_location' => 'Vatakara',
					'key' => 'Login'
					);
    	$log = $this->dashboard_model->log_entry($data);
    	if(!$log)
    	{
    		$err_data = array(
    						'date' => date('Y:m:d H:i:s'),
    						'data' => 'business_id: '.$id,
    						'error_msg' => 'failed to save business activity log'	
    						);
    		$this->dashboard_model->error_log($err_data);
    	}
    }

    function registration()
    {
    	$data = $this->session->flashdata('notification');
    	$this->load->view('merchant/signup',$data);
    }
    function validate_registration()
    {
		$this->load->library('form_validation');
    	$terms_conditions 	= $this->input->post('acceptTerms', TRUE);
    	if($terms_conditions == 'agreed')
    	{
    		if ($this->form_validation->run('signup') == FALSE)
			{
				$data['error_msg'] = validation_errors();
				$this->session->set_flashdata('notification',$data);
				redirect('/merchant/registration');
			}
			else
			{
				$reg_status = $this->dashboard_model->register($this->input->post());
				if($reg_status['reg_status'])
				{
					$data['success_msg'] = $reg_status['success_msg'];
					$this->session->set_flashdata('notification',$data);
					redirect('/merchant/login');
				}
				else
				{
					$data['error_msg'] = $reg_status['error_msg'];
					$this->session->set_flashdata('notification',$data);
					redirect('/merchant/registration');
				}
			}
    	}
    	else if($terms_conditions != '')
    	{
    		$data['error_msg'] = 'Please agree the terms and conditions';
			$this->session->set_flashdata('notification',$data);
			redirect('/merchant/registration');
    	}
	    else 
	    {
	    	$data['error_msg'] = 'Please complete the registration';
			$this->session->set_flashdata('notification',$data);
			redirect('/merchant/registration');
	    }
    	//echo $this->input->valid_ip($this->input->ip_address());*/
    	
    }

    function ajax_username_validate()
    {
    	$username = $this->input->post('user');
	    if($username != '')
	    {	
	    	$result = $this->dashboard_model->ajax_username_validate($username);
	    	if($result)
	    	{
	    		echo 'TRUE';
	    	}
	    	else echo 'FALSE';
	    }
	    else
	    {
	    	show_404();
	    }
    }
    
}

/* End of file dashboard.php */
/* Location: ./application/controllers/merchant/dashboard.php */