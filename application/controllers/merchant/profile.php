<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('merchant/profile_model');
		if(!$this->session->userdata('is_merchant_logged_in'))
        {
           redirect('/merchant/login');
        }
	}

	public function index()
	{
		$data['profile'] = $this->profile_model->get_details($this->session->userdata('business_id'));
		$data['details'] = $this->profile_model->get_adv_details($this->session->userdata('business_id'));
		if($this->session->flashdata('notification'))
		{
			$data[array_keys($this->session->flashdata('notification'))[0]] = $this->session->flashdata('notification');
		}
		$data['main_content'] = 'merchant/profile/main';
		$data['page_script'] = 'merchant/dashboard/script';
		$this->load->view('merchant/includes/template', $data);
	}

	function editprofile()
	{
		$segment = $this->uri->segment(4);
		if($segment == 'features')
		{
			if($this->input->post('area')!= '' OR $this->input->post('floors')!= '' OR $this->input->post('seating')!= '' OR $this->input->post('dining')!= '' OR $this->input->post('parking')!= '' OR $this->input->post('food_types')!= '')
			{
				$result = $this->profile_model->edit_features($this->input->post());
				if(!$result['status'])
				{
					if($result['error_type'] == 'FRAUD')
					{
						$data['error_msg'] = $result['error_msg'];
			            $this->session->set_flashdata('notification',$data);
			            redirect('/merchant/login');
					}
					else
					{
						$data['error_msg'] = $result['error_msg'];
			            $this->session->set_flashdata('notification',$data);
			            redirect('/merchant/profile');
					}

				}
				else
				{
					$data['success_msg'] = 'Changes updated';
					$this->session->set_flashdata('notification',$data);
					redirect('/merchant/profile');
				}
			}
			else echo 'no';	
		}
		
	}

	function merchant()
	{
		echo 'merchant profile';
	}

	function add_profile()
	{
		if($this->input->post())
		{
			echo 'g';
		}
		else
		{
			$data['main_content'] = 'merchant/profile/addprofile';
			$data['page_script'] = 'merchant/dashboard/script';
			$this->load->view('merchant/includes/template', $data);
		}	
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/merchant/dashboard.php */