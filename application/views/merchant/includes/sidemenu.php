<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>!N | <?php echo humanize($this->uri->segment(2)); ?></title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    

</head>

<body class="fixed-navigation">
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?php echo base_url(); ?>assets/img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $this->session->userdata('business_name'); ?></strong>
                             </span> <span class="text-muted text-xs block">Administrator <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php echo site_url('merchant/profile/merchant');?>">Profile</a></li>
                            <li><a href="<?php echo site_url('merchant/password/change');?>">Change Password</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo site_url('merchant/dashboard/logout');?>">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        !N
                    </div>
                </li>
                <li class="<?php if(uri_string() == 'merchant/dashboard') echo 'active';?>">
                    <a href="<?php echo site_url('merchant/dashboard');?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> </a>
                </li>
                <li class="<?php if(uri_string() == 'merchant/profile') echo 'active';?>">
                    <a href="<?php echo site_url('merchant/profile');?>"><i class="fa fa-user"></i> <span class="nav-label">Profile</span></a>
                </li>
                <li class="<?php if(uri_string() == 'merchant/events') echo 'active';?>">
                    <a href="<?php echo site_url('merchant/events');?>"><i class="fa fa-calendar"></i> <span class="nav-label">Events</span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="profile.html">Profile</a></li>
                    </ul>
                </li>
                <li class="<?php if(uri_string() == 'merchant/gallery') echo 'active';?>">
                    <a href="<?php echo site_url('merchant/gallery');?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Gallery </span></a>
                </li>
                <li class="<?php if(uri_string() == 'merchant/pinboard') echo 'active';?>">
                    <a href="<?php echo site_url('merchant/pinboard');?>"><i class="fa fa-thumb-tack"></i> <span class="nav-label">Pin board</span> </a>
                </li>
                <li class="<?php if(uri_string() == 'merchant/contract') echo 'active';?>">
                    <a href="<?php echo site_url('merchant/contract');?>"><i class="fa fa-edit"></i> <span class="nav-label">Contract</span></a>
                </li>
                <li class="<?php if(uri_string() == 'merchant/features') echo 'active';?>">
                    <a href="<?php echo site_url('merchant/features');?>"><i class="fa fa-desktop"></i> <span class="nav-label">App features</span>  <span class="pull-right label label-primary">SPECIAL</span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="projects.html">Projects</a></li>
                        <li><a href="project_detail.html">Project detail</a></li>
                        <li><a href="file_manager.html">File manager</a></li>
                        <li><a href="calendar.html">Calendar</a></li>
                        <li><a href="faq.html">FAQ</a></li>
                        <li><a href="timeline.html">Timeline</a></li>
                        <li><a href="pin_board.html">Pin board</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg ">