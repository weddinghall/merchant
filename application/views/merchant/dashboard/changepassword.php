        <?php if(isset($error_msg))
        { 
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $error_msg['error_msg']; ?>
        </div>
        <?php 
        } 
        if(isset($success_msg))
        { 
        ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $success_msg['success_msg']; ?>
        </div>
        <?php 
        }
        ?>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="align-center"><div class="col-lg-7 ">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Change Password</h5>
                            
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal" method="POST" action="<?php echo site_url('merchant/validate_change_password');?>">
                                <div class="form-group"><label class="col-lg-4 control-label">Current Password</label>

                                    <div class="col-lg-8"><input type="password" name="cur_password" placeholder="Current Password" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label">New Password</label>

                                    <div class="col-lg-8"><input type="password" name="new_password" placeholder="New Password" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label">Retype Password</label>

                                    <div class="col-lg-8"><input type="password" name="re_password" placeholder="Retype Password" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" type="submit">Change</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div></div>
            </div>
        </div>