            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Gallery</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('merchant/dashboard');?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('merchant/gallery');?>">Gallery</a>
                        </li>
                        <li class="active">
                            <strong>Add Images</strong>
                        </li>
                    </ol>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Dropzone Area</h5>
                        
                    </div>
                    <div class="ibox-content">
                        <form id="my-awesome-dropzone" class="dropzone" action="#">
                            <div class="dropzone-previews"></div>
                            <button type="submit" class="btn btn-primary pull-right">Submit this form!</button>
                        </form>
                        
                    </div>
                </div>
            </div>
            </div>

            </div>