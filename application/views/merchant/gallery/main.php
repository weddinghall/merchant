            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Gallery</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('merchant/dashboard');?>">Home</a>
                        </li>
                        <li class="active">
                            <strong>Gallery</strong>
                        </li>
                    </ol>
                </div>
            </div>
        <div class="wrapper wrapper-content">    
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <a href="<?php echo site_url('merchant/gallery/add');?>"><button class="btn btn-success m-t-n-xs btn-sm"><i class="fa fa-upload"></i> &nbsp;&nbsp;Add Images</button></a>
                            
                        </div>
                        <div class="ibox-content">
                            <div class="carousel slide" id="carousel3">
                                <div class="carousel-inner">
                                    <div class="item gallery active left">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <img alt="image" class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big1.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image" class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big2.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive"  src="<?php echo base_url(); ?>assets/img/p_big3.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big1.jpg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item gallery next left">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big3.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big1.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive"  src="<?php echo base_url(); ?>assets/img/p_big2.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big1.jpg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item gallery">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big2.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big3.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image"  class="img-responsive"  src="<?php echo base_url(); ?>assets/img/p_big1.jpg">
                                            </div>
                                            <div class="col-sm-6">
                                                <img alt="image" class="img-responsive" src="<?php echo base_url(); ?>assets/img/p_big2.jpg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a data-slide="prev" href="#carousel3" class="left carousel-control">
                                    <span class="icon-prev"></span>
                                </a>
                                <a data-slide="next" href="#carousel3" class="right carousel-control">
                                    <span class="icon-next"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>