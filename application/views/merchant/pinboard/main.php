            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Pin Board</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('merchant/dashboard');?>">Home</a>
                        </li>
                        <li class="active">
                            <strong>Pin Board</strong>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <p>
                            To buttons with any color or any size you can add extra icon on the left or the right side.
                        </p>

                        <h3 class="font-bold">Commom Icon Buttons</h3>
                        <p>
                            <button type="button" class="btn btn-w-m btn-primary" data-toggle="modal" data-target="#myModal5">Add</button>
                            <button type="button" class="btn btn-w-m btn-info" data-toggle="modal" data-target="#myModal4">View Demo</button>
                        </p>

                            
                    </div>
                </div>
            
                <div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">New Pin board</h4>
                            </div>
                            <form role="form" id="form">
                                <div class="modal-body">                                
                                    <div class="form-group"><label>Title</label> <input type="email" placeholder="Enter title" class="form-control" required></div>
                                    <div class="form-group"><label>Description</label> <textarea type="text" placeholder="Enter description" class="form-control" name="max" rows="5"></textarea></div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-w-m btn-primary">Pin</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal inmodal fade" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                
                            </div>
                            <div class="modal-body"> 
                            <ul class="notes">
                                <li>
                                    <div>
                                        <small>12:03:28 12-04-2014</small>
                                        <h4>Long established fact</h4>
                                        <p>The years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                                        <a href="#"><i class="fa fa-trash-o "></i></a>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <small>11:08:33 16-04-2014</small>
                                        <h4>Latin professor at Hampden-Sydney </h4>
                                        <p>The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                        <a href="#"><i class="fa fa-trash-o "></i></a>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <small>3:33:12 6-03-2014</small>
                                        <h4>The generated Lorem Ipsum </h4>
                                        <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                        <a href="#"><i class="fa fa-trash-o "></i></a>
                                    </div>
                                </li>      
                                </ul>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInUp">
                    <ul class="notes">
                        <li>
                            <div>
                                <small>12:03:28 12-04-2014</small>
                                <h4>Long established fact</h4>
                                <p>The years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                                <a href="#"><i class="fa fa-trash-o "></i></a>
                            </div>
                        </li>
                        <li>
                            <div>
                                <small>11:08:33 16-04-2014</small>
                                <h4>Latin professor at Hampden-Sydney </h4>
                                <p>The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                <a href="#"><i class="fa fa-trash-o "></i></a>
                            </div>
                        </li>
                        <li>
                            <div>
                                <small>9:12:28 10-04-2014</small>
                                <h4>The standard chunk of Lorem</h4>
                                <p>Ipsum used since the 1500s is reproduced below for those interested.</p>
                                <a href="#"><i class="fa fa-trash-o "></i></a>
                            </div>
                        </li>
                        <li>
                            <div>
                                <small>3:33:12 6-03-2014</small>
                                <h4>The generated Lorem Ipsum </h4>
                                <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                <a href="#"><i class="fa fa-trash-o "></i></a>
                            </div>
                        </li>
                        <li>
                            <div>
                                <small>5:20:11 4-04-2014</small>
                                <h4>Contrary to popular belief</h4>
                                <p>Hampden-Sydney College in Virginia, looked up one.</p>
                                <a href="#"><i class="fa fa-trash-o "></i></a>
                            </div>
                        </li>
                        <li>
                            <div>
                                <small>2:10:12 4-05-2014</small>
                                <h4>There are many variations</h4>
                                <p>All the Lorem Ipsum generators on the Internet .</p>
                                <a href="#"><i class="fa fa-trash-o "></i></a>
                            </div>
                        </li>
                        <li>
                            <div>
                                <small>10:15:26 6-04-2014</small>
                                <h4>Ipsum used standard chunk of Lorem</h4>
                                <p>Standard chunk  is reproduced below for those.</p>
                                <a href="#"><i class="fa fa-trash-o "></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>