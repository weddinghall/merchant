<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Business | Registration</title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">
   
</head>

<body class="gray-bg">
    <div class="loginscreen  animated fadeInDown">
        <div>
                <div class="ibox reg">
                    <div class="ibox-title">
                        <h5>Kalyanamandapam !n</h5>
                        <div class="ibox-tools">
                            <a href="<?php echo site_url('merchant/login');?>"><button class="btn btn-sm btn-outline btn-primary pull-right m-t-n-xs" type="button"><strong>Log in</strong></button></a>
                        </div>
                    </div>
                    <?php if(isset($error_msg))
                    { 
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php print_r($error_msg); ?>
                    </div>
                    <?php 
                    } ?>
                    <div class="ibox-content">
                        <h2>
                            Business Registration Form
                        </h2>

                        <form id="form" action="<?php echo site_url('merchant/validate_registration');?>" class="wizard-big" method="POST">
                            <h1>Account</h1>
                            <fieldset>
                                <h2>Account Information</h2>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label>Username *</label>
                                            <input id="username" name="username" type="text" class="form-control required">
                                            <p id="userstatus" style="color:red"></p>
                                        </div>
                                        <div class="form-group">
                                            <label>Password *</label>
                                            <input id="password" name="password" type="password" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password *</label>
                                            <input id="confirm" name="confirm" type="password" class="form-control required">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="text-center">
                                            <div style="margin-top: 20px">
                                                <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <h1>Business Profile</h1>
                            <fieldset>
                                <h2>Business Profile Information</h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Name *</label>
                                            <input id="businessname" name="businessname" type="text" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>Business type *</label>
                                            <select class="form-control m-b" name="businesstype">
                                                <option value="Auditorium">Auditorium / Convention hall</option>
                                                <option value="Travel">Bus / Travel</option>
                                            </select>
                                        </div>                                
                                        <div class="form-group">
                                            <label>District *</label>
                                            <input id="district" name="district" type="text" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile *</label>
                                            <input id="mobile" name="mobile" type="text" class="form-control required">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Location / Landmark *</label>
                                            <input id="landmark" name="landmark" type="text" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>City / Town *</label>
                                            <input id="city" name="city" type="text" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>Email (Recomended)</label>
                                            <input id="email" name="email" type="text" class="form-control  ">
                                        </div>
                                        <div class="form-group">
                                            <label>Landline </label>
                                            <input id="landline" name="landline" type="text" class="form-control ">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <h1>Personal Profile</h1>
                            <fieldset>
                                <h2>Personal Profile Information</h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Owner's name *</label>
                                            <input id="merchantname" name="merchantname" type="text" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>City *</label>
                                            <input id="merchantcity" name="merchantcity" type="text" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile *</label>
                                            <input id="merchantmobile" name="merchantmobile" type="text" class="form-control required">
                                        </div>                                            
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Address *</label>
                                            <input id="address" name="merchantaddress" type="text" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>District *</label>
                                            <input id="merchantdistrict" name="merchantdistrict" type="text" class="form-control required">
                                        </div>                                            
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="text-center">
                                            <div style="margin-top: 20px">
                                                <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <h1>Finish</h1>
                            <fieldset>
                                <h2>Terms and Conditions</h2>
                                <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required" value="agreed"> <label for="acceptTerms">I agree with the Terms and Conditions.</label>
                            </fieldset>
                        </form>
                    </div>
                </div>
                </div>
                
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/staps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/validate/jquery.validate.min.js"></script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            username:{
                                required: true,
                                minlength: 4
                            },
                            password:{
                                required: true,
                                minlength: 6
                            },
                            confirm: {
                                equalTo: "#password"
                            },
                            mobile:{
                                required: true,
                                digits: true,
                                minlength: 10,
                                maxlength: 10
                            },
                            landline:{
                                digits: true,
                                minlength: 11,
                                maxlength: 12
                            },
                            email:{
                                email: true
                            },
                            /*merchantmobile:{
                                required; true,
                                digits: true
                            }*/
                        }
                    });
            $('#username').focusout(function(event){
                    var username = $('#username').val();
                    if(username != '' && username.length>3)
                    {
                        $.ajax({
                                url: '<?php echo base_url(); ?>index.php/merchant/signup/ajax_username_validate',
                                dataType: 'text',
                                type: 'POST',
                                data: {'user':username},
                                success: function( data, textStatus, jQxhr ){
                                    if(data == 'FALSE')
                                    {
                                        $('#userstatus').text('Username not available');
                                        return false;
                                    }
                                    else
                                    {
                                        $('#userstatus').text(' ');
                                        return true;
                                    }
                                }, 
                                error:function( jqXhr, textStatus, errorThrown ){
                                }
                        });
                    }
            });

            
       });
    </script>

</body>

</html>
