<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Business Profile</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('merchant/dashboard');?>">Home</a>
                        </li>
                        <li >
                            <a href="<?php echo site_url('merchant/profile');?>">Profile</a>
                        </li>
                        <li class="active">
                            <strong>Add</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            
        <div class="wrapper wrapper-content">
            <div class="row animated fadeInRight">
                
                <div class="col-md-8">
                    <div class="ibox float-e-margins">                   
                        <div class="ibox-title">
                            <h5>Add Features</h5>
                                                  
                        </div>                    
                        <div class="ibox-content">
                            <div>
                                <div class="feed-activity-list">
                                    <div class="feed-element">
                                    <form method="POST" action="<?php echo site_url('merchant/profile/add_profile');?>">
                                        <div class="form-group"><label class="col-sm-3 control-label">Area of hall</label>

                                            <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="area" value="">
                                            <span class="input-group-addon">Sq.ft.</span></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">No of floors</label>

                                            <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="floors" value="">
                                            <span class="input-group-addon">No's</span></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">Seating capacity</label>

                                            <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="seating" value="">
                                            <span class="input-group-addon">No's</span></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">Dining capacity</label>

                                            <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="dining" value="">
                                            <span class="input-group-addon">No's</span></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">Parking capacity</label>

                                            <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="parking" value="">
                                            <span class="input-group-addon">Vehicles</span></div>
                                        </div>  
                                        <div class="form-group">
                                            <div class="col-sm-9 input-group m-b">
                                                <button class="btn btn-primary pull-right" type="submit">Save changes</button>
                                            </div>
                                        </div>  
                                    </form>                                   
                                    </div>
                                    
                                </div>
                            </div>                            
                        </div>
                    
                    </div>

                </div>
            </div>
        </div>