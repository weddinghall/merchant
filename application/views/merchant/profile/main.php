<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Business Profile</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('merchant/dashboard');?>">Home</a>
                        </li>
                        <li class="active">
                            <strong>Profile</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <?php if(isset($error_msg))
            { 
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $error_msg['error_msg']; ?>
            </div>
            <?php 
            } 
            if(isset($success_msg))
            { 
            ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $success_msg['success_msg']; ?>
            </div>
            <?php 
            }
            ?>
        <div class="wrapper wrapper-content">
            <div class="row animated fadeInRight">
                <div class="col-md-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Profile Detail</h5>
                            <div class="ibox-tools">
                                <button class="btn btn-primary m-t-n-xs btn-sm" data-toggle="modal" data-target="#myModal4"><i class="fa fa-edit"></i> Edit</button>
                                
                            </div>
                        </div>
                        <div style="position:relative;">
                            <div class="ibox-content no-padding border-left-right">
                                
                                <img alt="image" class="img-responsive" src="<?php echo base_url(); ?>assets/img/profile_big.jpg">
                                <a href="" class="btn btn-outline btn-info dim pos" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tooltip on bottom" type="button"><i class="fa fa-camera"></i></a>
                                
                            </div>
                            <div class="ibox-content profile-content">
                                <h4><strong><?php echo $profile->business_name;?></strong></h4>
                                <p><i class="fa fa-map-marker"></i><?php echo ' '.$profile->business_location.', '.$profile->business_city;?></p>
                                <p><i class="fa fa-home"></i><?php echo ' '.$profile->business_district;?></p>
                                <p><i class="fa fa-mobile"></i><?php echo ' '.$profile->business_mobile;?></p>
                                <p><i class="fa fa-tty"></i><?php echo ' '.$profile->business_landline;?></p>
                                <p><i class="fa fa-envelope"></i><?php echo ' '.$profile->business_email;?></p>
                                <p><i class="fa fa-briefcase"></i><?php echo ' '.$profile->account_type.' Account'; if($profile->account_type_no == 0){?><button type="button" class="flt btn btn-warning btn-xs">Upgrade</button><?php } ?></p>
                                
                                <div class="row m-t-lg">
                                    <div class="col-md-4">
                                        <span class="bar">5,3,9,6,5,9,7,3,5,2</span>
                                        <h5><strong>169</strong> Posts</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                        <h5><strong>28</strong> Following</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="bar">5,3,2,-1,-3,-2,2,3,5,2</span>
                                        <h5><strong>240</strong> Followers</h5>
                                    </div>
                                </div>
                                <div class="user-button">
                                    <div class="row">
                                        <a class="btn btn-primary btn-rounded btn-block" href="<?php echo site_url('merchant/gallery');?>"><i class="fa fa-plus" ></i> &nbsp;Add images in your gallery</a>
                                        
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
                <div class="col-md-8">
                    <div class="ibox float-e-margins">
                    <?php if(empty($details))
                    {
                    ?>
                        <div class="ibox-title">
                           
                            <div class="ibox-tools">
                                <a href="<?php echo site_url('merchant/profile/add');?>"class="btn btn-block btn-success m-t-n-xs btn-sm white"><i class="fa fa-plus"></i> Add features</a>
                                
                            </div>
                            
                        </div>
                    <?php 
                    }
                    else
                    {
                    ?>
                        <div class="ibox-title">
                            <h5>Activites</h5>
                            <div class="ibox-tools">
                                <button class="btn btn-primary m-t-n-xs btn-sm" data-toggle="modal" data-target="#myModal4"><i class="fa fa-edit"></i> Edit</button>
                                
                            </div>                        
                        </div>                    
                        <div class="ibox-content">
                            <div>
                                <div class="feed-activity-list">
                                    <div class="feed-element">
                                       <div class="form-group"><label class="col-lg-4 control-label">Area of hall</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo $details->area." Sq.ft";?></p></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label">Floors</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo $details->floors;?></p></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label">Seating capacity</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo $details->seating_capacity." seats";?></p></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label">Dining capacity</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo $details->dining_capacity." seats";?></p></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label">Parking capacity</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo $details->parking_capacity." vehicles";?></p></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label">Rate per day</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo "Rs ".$details->rate_per_day."/-";?></p></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label">Advance</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo "Rs ".$details->advance_amt."/-";?></p></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label">Types of food</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo $details->types_of_food;?></p></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-4 control-label">Facilities</label>
                                            <div class="col-lg-8"><p class="form-control-static"><?php echo $details->facilities;?></p></div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>

                            <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated fadeIn">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title">Edit profile</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action = "<?php echo site_url('merchant/profile/editprofile/features');?>" class="form-horizontal">
                                            <div class="form-group"><label class="col-sm-3 control-label">Area of hall</label>

                                                <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="area" value="<?php echo $details->area;?>">
                                                <span class="input-group-addon">Sq.ft.</span></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-3 control-label">No of floors</label>

                                                <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="floors" value="<?php echo $details->floors;?>">
                                                <span class="input-group-addon">No's</span></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-3 control-label">Seating capacity</label>

                                                <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="seating" value="<?php echo $details->seating_capacity;?>">
                                                <span class="input-group-addon">No's</span></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-3 control-label">Dining capacity</label>

                                                <div class="col-sm-6 input-group m-b"><input type="text" class="form-control" name="dining" value="<?php echo $details->dining_capacity;?>">
                                                <span class="input-group-addon">No's</span></div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-3 control-label">Parking capacity</label>

                                                <div class="col-lg-6 input-group m-b"><input type="text" class="form-control" name="parking" value="<?php echo $details->parking_capacity;?>">
                                                <span class="input-group-addon">Vehicles</span></div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-3 control-label">Types of food</label>

                                                <div class="col-sm-10">
                                                <label class="checkbox-inline"><input type="checkbox" value="option1" id="inlineCheckbox1" name="food_types"> Sadya </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option2" id="inlineCheckbox2" name="food_types"> South Indian Non-veg </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option3" id="inlineCheckbox3" name="food_types"> North Indian </label>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-3 control-label">Facilities</label>

                                                <div class="col-sm-10">
                                                <label class="checkbox-inline"><input type="checkbox" value="option1" id="inlineCheckbox1" name="facilities"> Free Wifi </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option2" id="inlineCheckbox2" name="facilities"> Centralised A/c </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option3" id="inlineCheckbox3" name="facilities"> River/Beach front </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option1" id="inlineCheckbox1" name="facilities"> Eco-friendly atmosphere </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option2" id="inlineCheckbox2" name="facilities"> Basement parking </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option3" id="inlineCheckbox3" name="facilities"> Lounges/Bars </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option2" id="inlineCheckbox2" name="facilities"> Airport shuttle srvice </label>
                                                <label class="checkbox-inline"><input type="checkbox" value="option3" id="inlineCheckbox3" name="facilities"> Courtyard </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                    </div>

                </div>
            </div>
        </div>